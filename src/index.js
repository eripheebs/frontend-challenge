import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store/configureStore';
import statuses from './constants/statuses';

import 'bootstrap/dist/css/bootstrap.min.css';
import './scss/main.css';

const initialState = {
    transactionFeed: {
        transactions: [],
        page: 1,
        endOfScroll: false,
        filter: statuses.ALL,
        fitleredTransactions: []
    },
    transaction: {
        details: {}
    },
    user: {
        details: {}
    }
};

const store = configureStore(initialState);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();

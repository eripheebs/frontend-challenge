import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import TransactionFeedContainer from './Containers/TransactionFeedContainer';
import TransactionContainer from './Containers/TransactionContainer';
import Header from './presentation/Header';
import PathNotFound from './presentation/PathNotFound';

class App extends Component {
    render() {
        return (
            <Router>
                <div className='container'>
                    <Header />
                    <Switch>
                        <Route exact path='/' component={TransactionFeedContainer} />
                        <Route path='/transaction/:id' component={TransactionContainer} />
                        <Route component={PathNotFound} />
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;


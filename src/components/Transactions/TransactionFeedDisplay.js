import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TransactionSummary from './TransactionSummary';
import InfiniteScroll from 'react-infinite-scroll-component';
import content from '../../constants/content';
import statuses from '../../constants/statuses';

export default class TransactionFeedDisplay extends Component {
    render() {
        const {
            transactions,
            filteredTransactions,
            filter
        } = this.props;

        const visibleTransactions = (filter === statuses.ALL) ? transactions : filteredTransactions;

        const listItems = visibleTransactions &&
            visibleTransactions.length > 0 &&
            visibleTransactions.map(transaction => {
                const {
                    id,
                    lenderId,
                    itemId,
                    fromDate,
                    toDate,
                    status
                } = transaction;
                return (
                    <TransactionSummary
                        key={id}
                        id={id}
                        lenderId={lenderId}
                        itemId={itemId}
                        fromDate={fromDate}
                        toDate={toDate}
                        status={status}
                        history={this.props.history}
                    />
                );
            });

        return (
            <InfiniteScroll
                dataLength={listItems.length}
                next={this.props.getNextPage}
                hasMore={!this.props.endOfScroll}
                loader={<h4>{content.loading}</h4>}
                endMessage={
                    <p>{content.endOfFeed}</p>
                }
            >
                {listItems}
            </InfiniteScroll>
        );
    }
}

const transactionPropType = PropTypes.shape({
    id: PropTypes.number.isRequired,
    lenderId: PropTypes.number.isRequired,
    itemId: PropTypes.number.isRequired,
    fromDate: PropTypes.string.isRequired,
    toDate: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    promocode: PropTypes.string,
    creditUsed: PropTypes.number,
    price: PropTypes.number,
    totalDiscount: PropTypes.number,
    currency: PropTypes.string
})

const propTypes = {
    history: PropTypes.object.isRequired,
    transactions: PropTypes.arrayOf(
      transactionPropType
    ).isRequired,
    filteredTransactions: PropTypes.arrayOf(
        transactionPropType
    ),
    getNextPage: PropTypes.func.isRequired,
    endOfScroll: PropTypes.bool.isRequired,
    filter: PropTypes.string.isRequired
};

TransactionFeedDisplay.propTypes = propTypes;
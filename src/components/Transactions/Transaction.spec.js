import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Transaction from './Transaction';

Enzyme.configure({ adapter: new Adapter() });

const status = 'fake status';
const props = {
    transaction: {
        details: {
            id: 1,
            status
        }
    },
    match: {
        params: {
            id: 1
        }
    },
    store: {
        getState: jest.fn()
    },
    fetchTransaction: jest.fn()
};

const spyOnFetchTransaction = jest.spyOn(props, 'fetchTransaction');

const setup = () => {
    const enzymeWrapper = mount(<Transaction {...props} />);

    return {
        props,
        enzymeWrapper
    };
};

describe('Component: Transaction', () => {
    it('should render self', () => {
        const { enzymeWrapper } = setup();
        expect(enzymeWrapper.find('div').first().hasClass('c-transaction')).toBe(true);
    });

    it('should render subcomponent StatusDropdown', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('StatusDropdown')).toHaveLength(1);
    });

    it('should pass props to StatusDropdown', () => {
        const { enzymeWrapper } = setup();

        const statusDropdownComponent = enzymeWrapper.find('StatusDropdown').props();
        expect(statusDropdownComponent.status).toBe(status)
    });

    it('should call fetchTransaction at componentDidMount', () => {
        const { enzymeWrapper, props } = setup();
        expect(spyOnFetchTransaction).toHaveBeenCalled();
    });
});

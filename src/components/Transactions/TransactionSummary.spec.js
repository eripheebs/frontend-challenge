import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TransactionSummary from './TransactionSummary';

Enzyme.configure({ adapter: new Adapter() });

const status = 'fake status';
const id = Math.random(10);
const lenderId = Math.random(10);
const itemId = Math.random(10);
const fromDate = 'Fake date';
const toDate = 'Fake date 2';

const props = {
    history: {},
    id,
    lenderId,
    itemId,
    fromDate,
    toDate,
    status
};

const setup = () => {
    const enzymeWrapper = mount(<TransactionSummary {...props} />);

    return {
        props,
        enzymeWrapper
    };
};

describe('Component: TransactionFeedDisplay', () => {
    it('should render self', () => {
        const { enzymeWrapper } = setup();
        expect(enzymeWrapper.find('div').hasClass('c-transaction__summary')).toBe(true);
    });
});
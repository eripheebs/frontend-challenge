import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TransactionFeedDisplay from './TransactionFeedDisplay';

Enzyme.configure({ adapter: new Adapter() });

const status = 'fake status';
const END_OF_SCROLL = false;
const id = Math.random(10);
const lenderId = Math.random(10);
const itemId = Math.random(10);
const fromDate = 'Fake date';
const toDate = 'Fake date 2';

const transaction = {
    id,
    lenderId,
    itemId,
    fromDate,
    toDate,
    status
}

const props = {
    transactions: [
        transaction
    ],
    fetchTransactions: jest.fn(),
    filterTransactions: jest.fn(),
    filteredTransactions: [],
    page: 1,
    filter: status,
    history: {},
    endOfScroll: END_OF_SCROLL,
    getNextPage: jest.fn()
};

const setup = () => {
    const enzymeWrapper = mount(<TransactionFeedDisplay {...props} />);

    return {
        props,
        enzymeWrapper
    };
};

describe('Component: TransactionFeedDisplay', () => {
    it('should render subcomponent InfiniteScroll', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('InfiniteScroll')).toHaveLength(1);
    });

    it('should pass props to InfiniteScroll', () => {
        const { enzymeWrapper } = setup();

        const transactionSummaryComponent = enzymeWrapper.find('InfiniteScroll').props();
        expect(transactionSummaryComponent.hasMore).toBe(!END_OF_SCROLL);
    });
});
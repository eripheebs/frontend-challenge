import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import StatusDropdown from './StatusDropdown';

Enzyme.configure({ adapter: new Adapter() });

const status = 'fake status';
const props = {
    onSelect: jest.fn(),
    showAll: true,
    status
};

const setup = () => {
    const enzymeWrapper = mount(<StatusDropdown {...props} />);

    return {
        props,
        enzymeWrapper
    };
};

describe('Component: StatusDropdown', () => {
    it('should render self', () => {
        const { enzymeWrapper } = setup();
        expect(enzymeWrapper.find('select').hasClass('c-transaction__dropdown')).toBe(true);
    });

    it('should call onSelect prop if own onSelect method called', () => {
        const { enzymeWrapper, props } = setup();
        const event = {
            target: {
                value: status
            }
        };
        enzymeWrapper.instance().onSelect(event);
        expect(props.onSelect.mock.calls[0][0]).toBe(status);
    });
});

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import statuses from '../../constants/statuses';

export default class StatusDropdown extends Component {
    constructor() {
        super();

        this.onSelect = this.onSelect.bind(this);
    }

    onSelect(event) {
        this.props.onSelect(event.target.value);
    }

    render() {
        let statusValues = Object.values(statuses);
        if (!this.props.showAll) {
            statusValues = statusValues.filter(value => value !== statuses.ALL);
        }
        const { status } = this.props;

        return (
            <select
                className='c-transaction__dropdown form-control form-control-lg'
                onChange={this.onSelect}
                value={status}
            >
                {
                    statusValues.map(stat => {
                        return (
                            <option
                                key={stat}
                                value={stat}
                            >
                                {stat}
                            </option>
                        );
                    })
                }
            </select>
        );
    }
}

const propTypes = {
    onSelect: PropTypes.func.isRequired,
    status: PropTypes.string,
    showAll: PropTypes.bool
};

StatusDropdown.propTypes = propTypes;

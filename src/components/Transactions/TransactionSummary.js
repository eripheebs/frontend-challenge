import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ListItem from '../presentation/ListItem';
import content from '../../constants/content';

const labels = content.transactionDetails;

class TransactionSummary extends Component {
    render() {
        const {
            id,
            lenderId,
            itemId,
            status
        } = this.props;

        return (
            <div className='c-transaction__summary row align-items-center'>
                <label className='col-12 col-lg-1'>
                    {id}
                </label>
                <ul className='list-group list-group-flush col-12 col-lg-6'>
                    <ListItem label={labels.lenderId} content={lenderId} />
                    <ListItem label={labels.itemId} content={itemId} />
                    <ListItem label={labels.status} content={status} />
                </ul>
                <button
                    className='btn btn-outline-secondary'
                    onClick={()=>this.props.history.push(`/transaction/${id}`)}
                >
                    {content.viewDetails}
                </button>
            </div>
        );
    }
}

const propTypes = {
    history: PropTypes.object.isRequired,
    id: PropTypes.number.isRequired,
    lenderId: PropTypes.number.isRequired,
    itemId: PropTypes.number.isRequired,
    fromDate: PropTypes.string.isRequired,
    toDate: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    promocode: PropTypes.string,
    creditUsed: PropTypes.number,
    price: PropTypes.number,
    totalDiscount: PropTypes.number,
    currency: PropTypes.string
};

TransactionSummary.propTypes = propTypes;

export default TransactionSummary;
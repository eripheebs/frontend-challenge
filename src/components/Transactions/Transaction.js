import React, { Component } from 'react';
import PropTypes from 'prop-types';
import StatusDropdown from './StatusDropdown';
import content from '../../constants/content';
import ListItem from '../presentation/ListItem';
import UserContainer from '../Containers/UserContainer';

const labels = content.transactionDetails;

class Transaction extends Component {
    componentDidMount() {
        const { id } = this.props.match.params;

        this.props.fetchTransaction(id);
    }

    render() {
        const {
            id,
            lenderId,
            itemId,
            fromDate,
            toDate,
            status,
            promocode,
            creditUsed,
            price,
            totalDiscount,
            currency
        } = this.props.transaction.details;

        const selectTransactionStatus = (status) => {
            this.props.changeTransactionStatus(id, status)
        }

        return (
            <div className='c-transaction row'>
                <ul className='list-group list-group-flush col-12 col-lg-6'>
                    <ListItem label={labels.id} content={id} />
                    <ListItem label={labels.status} content={status} />
                    <ListItem label={labels.lenderId} content={lenderId} />
                    <ListItem label={labels.itemId} content={itemId} />
                    <ListItem label={labels.fromDate} content={fromDate} />
                    <ListItem label={labels.toDate} content={toDate} />
                    <ListItem label={labels.promocode} content={promocode} />
                    <ListItem label={labels.creditUsed} content={creditUsed} />
                    <ListItem label={labels.price} content={price} />
                    <ListItem label={labels.totalDiscount} content={totalDiscount} />
                    <ListItem label={labels.currency} content={currency} />
                </ul>
                <div className='col-12 col-lg-4'>
                    {lenderId ? <UserContainer id={lenderId}/> : '' }
                    <button
                        className='btn btn-outline-secondary c-transaction__button'
                        onClick={()=>this.props.history.push('/')}
                    >
                        {content.back}
                    </button>
                    <p>{content.changeTransactionStatus}</p>
                    <StatusDropdown
                        status={status}
                        onSelect={selectTransactionStatus}
                    />
                </div>
            </div>
        );
    }
}

const propTypes = {
    match: PropTypes.shape({
        params: PropTypes.object.isRequired
    }),
    transaction: PropTypes.shape({
        details: PropTypes.shape({
            id: PropTypes.number,
            lenderId: PropTypes.number,
            itemId: PropTypes.number,
            fromDate: PropTypes.string,
            toDate: PropTypes.string,
            status: PropTypes.string,
            promocode: PropTypes.string,
            creditUsed: PropTypes.number,
            price: PropTypes.number,
            totalDiscount: PropTypes.number,
            currency: PropTypes.string
        })
    }),
    fetchTransaction: PropTypes.func.isRequired
};

Transaction.propTypes = propTypes;

export default Transaction;
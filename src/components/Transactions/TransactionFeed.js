import React, { Component } from 'react';
import PropTypes from 'prop-types';
import StatusDropdown from './StatusDropdown';
import TransactionFeedDisplay from './TransactionFeedDisplay';
import content from '../../constants/content';
import statuses from '../../constants/statuses';

class TransactionFeed extends Component {
    componentDidMount() {
        this.props.fetchTransactions();
    }

    render() {
        const {
            transactions,
            page,
            endOfScroll,
            filter,
            filterTransactions,
            filteredTransactions
        } = this.props;

        const getNextPage = async () => {
            await this.props.fetchTransactions(page + 1);

            if (filter !== statuses.ALL) {
                this.props.filterTransactions(filter);
            }
        }

        return (
            <div className='c-feed__display col-12'>
                {content.filterTransactions}
                <StatusDropdown
                    status={filter}
                    onSelect={filterTransactions}
                    showAll={true}
                />
                <TransactionFeedDisplay
                    transactions={transactions}
                    filteredTransactions={filteredTransactions}
                    getNextPage={getNextPage}
                    endOfScroll={endOfScroll}
                    filter={filter}
                    history={this.props.history}
                />
            </div>
        );
    }
}

const propTypes = {
    transactions: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        lenderId: PropTypes.number.isRequired,
        itemId: PropTypes.number.isRequired,
        fromDate: PropTypes.string.isRequired,
        toDate: PropTypes.string.isRequired,
        status: PropTypes.string.isRequired,
        promocode: PropTypes.string,
        creditUsed: PropTypes.number,
        price: PropTypes.number,
        totalDiscount: PropTypes.number,
        currency: PropTypes.string
      })
    ).isRequired,
    page: PropTypes.number.isRequired,
    fetchTransactions: PropTypes.func.isRequired
};

TransactionFeed.propTypes = propTypes;

export default TransactionFeed;
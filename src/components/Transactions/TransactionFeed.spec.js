import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TransactionFeed from './TransactionFeed';

Enzyme.configure({ adapter: new Adapter() });

const status = 'fake status';
const props = {
    transactions: [],
    fetchTransactions: jest.fn(),
    filterTransactions: jest.fn(),
    filteredTransactions: [],
    page: 1,
    filter: status,
    history: {},
    endOfScroll: false
};

const spyOnFetchTransactions = jest.spyOn(props, 'fetchTransactions');

const setup = () => {
    const enzymeWrapper = mount(<TransactionFeed {...props} />);

    return {
        props,
        enzymeWrapper
    };
};

describe('Component: TransactionFeed', () => {
    it('should render self', () => {
        const { enzymeWrapper } = setup();
        expect(enzymeWrapper.find('div').first().hasClass('c-feed__display')).toBe(true);
    })

    it('should call fetchTransactions at componentDidMount', () => {
        const { props } = setup();
        expect(spyOnFetchTransactions).toHaveBeenCalled();
    });

    it('should render subcomponent StatusDropdown', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('StatusDropdown')).toHaveLength(1);
    });

    it('should pass props to StatusDropdown', () => {
        const { enzymeWrapper } = setup();

        const statusDropdownComponent = enzymeWrapper.find('StatusDropdown').props();
        expect(statusDropdownComponent.status).toBe(status);
    });
});
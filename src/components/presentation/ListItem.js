import React from 'react';

const ListItem = ({label, content}) =>
    <li className='list-group-item d-flex justify-content-between align-items-center'>
        {label}
        <span className='badge badge-primary badge-pill'>
            {content}
        </span>
    </li>;

export default ListItem;
import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Header from './Header';

Enzyme.configure({ adapter: new Adapter() });

describe('Component: Header', () => {
    it('should render self', () => {
        const enzymeWrapper = mount(<Header />);

        expect(enzymeWrapper.find('div').hasClass('c-header')).toBe(true);
    });
});

import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PathNotFound from './Header';

Enzyme.configure({ adapter: new Adapter() });

describe('Component: Header', () => {
    it('should render self', () => {
        const enzymeWrapper = mount(<PathNotFound />);

        expect(enzymeWrapper.find('h1')).toHaveLength(1);
    });
});

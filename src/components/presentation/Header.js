import React, {Component} from 'react';
import content from '../../constants/content';

const feedLogo = require('../../img/feed_logo.svg');

export default class Header extends Component {
    render() {

      return (
        <div className='c-header'>
            <h1>{content.title}</h1>
            <img src={feedLogo} alt='' />
        </div>
      );
    }
}

import React from 'react';
import content from '../../constants/content';

const PathNotFound = () =>
    <h1>{content.pathNotFound}</h1>;

export default PathNotFound;
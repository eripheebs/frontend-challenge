import { connect } from 'react-redux';
import TransactionFeed from '../Transactions/TransactionFeed';
import {
    fetchTransactions,
    filterTransactions
}from '../../actions/transactionFeed';

export const mapStateToProps = state => ({
    transactions: state.transactionFeed.transactions,
    page: state.transactionFeed.page,
    endOfScroll: state.transactionFeed.endOfScroll,
    filter: state.transactionFeed.filter,
    filteredTransactions: state.transactionFeed.filteredTransactions
});

export const mapDispatchToProps = dispatch => ({
    fetchTransactions: (page) => dispatch(fetchTransactions(page)),
    filterTransactions: (status) => dispatch(filterTransactions(status))
});

const TransactionFeedContainer = connect(mapStateToProps, mapDispatchToProps)(TransactionFeed);

export default TransactionFeedContainer;
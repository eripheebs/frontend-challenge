import { connect } from 'react-redux';
import Transaction from '../Transactions/Transaction'
import {
    fetchTransaction,
    changeTransactionStatus
} from '../../actions/transaction';

export const mapStateToProps = state => ({
    transaction: state.transaction
});

export const mapDispatchToProps = dispatch => ({
    fetchTransaction: (id) => dispatch(fetchTransaction(id)),
    changeTransactionStatus: (id, status) => dispatch(changeTransactionStatus(id, status))
});

const TransactionContainer = connect(mapStateToProps, mapDispatchToProps)(Transaction);

export default TransactionContainer;

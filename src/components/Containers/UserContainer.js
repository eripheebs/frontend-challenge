import { connect } from 'react-redux';
import User from '../User/User'
import { fetchUser } from '../../actions/user';

export const mapStateToProps = (state, ownProps) => ({
    user: state.user,
    id: ownProps.id
});

export const mapDispatchToProps = dispatch => ({
    fetchUser: (id) => dispatch(fetchUser(id))
});

const UserContainer = connect(mapStateToProps, mapDispatchToProps)(User);

export default UserContainer;
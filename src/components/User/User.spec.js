import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import User from './User';

Enzyme.configure({ adapter: new Adapter() });

const props = {
    id: 1,
    user: {
        details: {}
    },
    store: {
        getState: jest.fn()
    },
    fetchUser: jest.fn()
};

const spyOnFetchUser = jest.spyOn(props, 'fetchUser');

const setup = () => {
    const enzymeWrapper = mount(<User {...props} />);

    return {
        props,
        enzymeWrapper
    };
};

describe('Component: User', () => {
    it('should render self', () => {
        const { enzymeWrapper } = setup();
        expect(enzymeWrapper.find('div').first().hasClass('c-user')).toBe(true);
    });

    it('should call fetchUser at componentDidMount', () => {
        const { enzymeWrapper, props } = setup();
        expect(spyOnFetchUser).toHaveBeenCalled();
    });
});

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import content from '../../constants/content';
import ListItem from '../presentation/ListItem';

const labels = content.userDetails;

class User extends Component {
    componentDidMount() {
        const { id } = this.props;
        this.props.fetchUser(id);
    }

    render() {
        const {
            firstName,
            lastName,
            email,
            credit,
            telephone
        } = this.props.user.details;

        return (
            <div className='c-user row'>
                <div className='c-user__name'>
                    <span>{content.userTitle}</span>
                    <span>{firstName}</span>
                    <span>{lastName}</span>
                </div>
                <ul className='list-group list-group-flush col-12'>
                    <ListItem label={labels.email} content={email} />
                    <ListItem label={labels.credit} content={credit} />
                    <ListItem label={labels.telephone} content={telephone} />
                </ul>
            </div>
        );
    }
}

const propTypes = {
    id: PropTypes.number,
    user: PropTypes.shape({
        details: PropTypes.shape({
            firstName: PropTypes.string,
            lastName: PropTypes.string,
            email: PropTypes.string,
            credit: PropTypes.number,
            profileImgUrl: PropTypes.string,
            telephone: PropTypes.string
        })
    }),
    fetchUser: PropTypes.func.isRequired
};

User.propTypes = propTypes;

export default User;
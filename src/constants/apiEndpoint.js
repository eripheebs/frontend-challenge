const API_ENDPOINT = 'http://localhost:8080';

export const TRANSACTIONS_ENDPOINT = `${API_ENDPOINT}/transactions/`;
export const TRANSACTION_ENDPOINT = `${API_ENDPOINT}/transaction/`;
export const USER_ENDPOINT = `${API_ENDPOINT}/user/`;
const statuses = {
    PAID: 'PAID',
    PRE_AUTHORIZED: 'PRE_AUTHORIZED',
    PRE_AUTHORIZED_CANCELLED: 'PRE_AUTHORIZED_CANCELLED',
    ESCROW: 'ESCROW',
    ALL: 'ALL'
};

export default statuses;

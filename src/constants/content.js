const content = {
    title: 'Feed',
    pathNotFound: '404',
    changeTransactionStatus: 'Change Transaction Status',
    endOfFeed: 'You have reached the end of feed',
    loading: 'Loading...',
    filterTransactions: 'Filter Transactions',
    viewDetails: 'View Details',
    back: 'Back',
    userTitle: 'User',
    transactionDetails: {
        id: 'ID ',
        lenderId: 'Lender ID',
        itemId: 'Item ID',
        fromDate: 'From',
        toDate: 'To',
        status: 'Status',
        promocode: 'Promo code',
        creditUsed: 'Credit used',
        price: 'Price',
        totalDiscount: 'Total Discount',
        currency: 'Currency'
    },
    userDetails: {
        email: 'Email',
        credit: 'Credit',
        telephone: 'Telephone'
    }
};

export default content;

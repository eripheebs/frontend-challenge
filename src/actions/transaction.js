import { GET_TRANSACTION } from '../constants/actionTypes';
import { TRANSACTION_ENDPOINT } from '../constants/apiEndpoint';
import fetchHelper from './fetchHelper';
import putData from '../api/putData';

const getTransaction = (details) => ({
    type: GET_TRANSACTION,
    details
});

export const fetchTransaction = (id) => (dispatch) => {
    const url = `${TRANSACTION_ENDPOINT}${id}`;

    dispatch(fetchHelper(url, getTransaction));
};

const putTransaction = (
    id,
    status
) => async (dispatch) => {
    const url = `${TRANSACTION_ENDPOINT}${id}`;;

    const details = await putData(url, status);

    dispatch(getTransaction(details));
};

const shouldUpdateStatus = (state, status) => {
    return state.transaction.status !== status;
};

export const changeTransactionStatus = (id, status) => (dispatch, getState) => {
    const state = getState();
    if (shouldUpdateStatus(state, status)) {
        return dispatch(putTransaction(id, status));
    }
};

import { GET_USER } from '../constants/actionTypes';
import { USER_ENDPOINT } from '../constants/apiEndpoint';
import fetchHelper from './fetchHelper';

const getUser = (details) => ({
    type: GET_USER,
    details
});

export const fetchUser = (id) => async (dispatch) => {
    const url = `${USER_ENDPOINT}${id}`;

    dispatch(fetchHelper(url, getUser));
};
import { GET_TRANSACTIONS,
    FILTER_TRANSACTONS,
    ADD_TRANSACTIONS_TO_STATE
} from '../constants/actionTypes';
import { TRANSACTIONS_ENDPOINT } from '../constants/apiEndpoint';
import fetchData from '../api/fetchData';

const getTransactions = (transactions) => ({
    type: GET_TRANSACTIONS,
    transactions
});

const scrollReceiveMoreTransactions = (
    transactions,
    page
) => ({
    type: ADD_TRANSACTIONS_TO_STATE,
    transactions,
    page
});

export const filterTransactions = (status) => ({
    type: FILTER_TRANSACTONS,
    status
});

const addOrReplaceTransactions = (
    dispatch,
    transactionFeed,
    page
) => {
    if (page > 1) {
        return dispatch(
            scrollReceiveMoreTransactions(
                transactionFeed,
                page
            )
        );
    }
    dispatch(getTransactions(transactionFeed, page));
}

export const fetchTransactions = (page = 1) => async (dispatch) => {
    const url = `${TRANSACTIONS_ENDPOINT}${page}`;

    const transactionFeedData = await fetchData(url);
    const transactionFeed = transactionFeedData ? transactionFeedData : [];

    addOrReplaceTransactions(
        dispatch,
        transactionFeed,
        page
    );
};


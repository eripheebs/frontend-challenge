import {
    fetchTransactions,
    filterTransactions
} from './transactionFeed';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
    GET_TRANSACTIONS,
    ADD_TRANSACTIONS_TO_STATE,
    FILTER_TRANSACTONS
} from '../constants/actionTypes';
import { TRANSACTIONS_ENDPOINT } from '../constants/apiEndpoint';
import axios from 'axios';
import axiosMockAdapter from 'axios-mock-adapter';
import transaction from '../reducers/transaction';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Actions:', () => {
    const mockAdapter = new axiosMockAdapter(axios);
    const status = 'Fake Status';
    const transactions = [];

    beforeAll(() => {
        mockAdapter.reset();
    });

    describe('fetchTransactions', () => {
        it('should create GET_TRANSACTIONS when fetching transactions has been done and page is 1', async () => {
            expect.assertions(1);
            const page = 1;
            const url = `${TRANSACTIONS_ENDPOINT}${page}`

            const expectedActions = [
                {
                    type: GET_TRANSACTIONS,
                    transactions
                }
            ];

            const store = mockStore({ transactionFeed: {} });

            mockAdapter.onGet(url).reply(200, transactions);

            await store.dispatch(fetchTransactions(page));

            expect(store.getActions()).toEqual(expectedActions);
        });

        it('should create ADD_TRANSACTIONS_TO_STATE when fetching transactions has been done and page is > 1', async () => {
            expect.assertions(1);
            const page = 2;
            const url = `${TRANSACTIONS_ENDPOINT}${page}`

            const expectedActions = [
                {
                    type: ADD_TRANSACTIONS_TO_STATE,
                    transactions,
                    page
                }
            ];

            const store = mockStore({ transactionFeed: {} });

            mockAdapter.onGet(url).reply(200, transactions);

            await store.dispatch(fetchTransactions(page));

            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    describe('filterTransactions', () => {
        it('should create FILTER_TRANSACTIONS', () => {
            const expectedActions = [
                {
                    type: FILTER_TRANSACTONS,
                    status
                }
            ];

            const store = mockStore({ transactionFeed: {} });

            store.dispatch(filterTransactions(status));

            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});
import {
    fetchTransaction,
    changeTransactionStatus
} from './transaction';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { GET_TRANSACTION } from '../constants/actionTypes';
import { TRANSACTION_ENDPOINT } from '../constants/apiEndpoint';
import axios from 'axios';
import axiosMockAdapter from 'axios-mock-adapter';
import fetchHelper from './fetchHelper';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Actions:', () => {
    const mockAdapter = new axiosMockAdapter(axios);
    const id = 1;
    const url = `${TRANSACTION_ENDPOINT}${id}`
    const details = {
        id: 'fake transaction id'
    };

    beforeAll(() => {
        mockAdapter.reset();
    });

    describe('changeTransactionStatus', () => {
        it('should create GET_TRANSACTION when putting transaction status has been done', async () => {
            expect.assertions(1);

            const status = 'FAKE STATUS';

            const expectedActions = [
                {
                    type: GET_TRANSACTION,
                    details
                }
            ];

            const store = mockStore({ transaction: {} });

            mockAdapter.onPut(url).reply(200, details);

            await store.dispatch(changeTransactionStatus(id, status));

            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});
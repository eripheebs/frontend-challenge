import fetchHelper from './fetchHelper';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import axiosMockAdapter from 'axios-mock-adapter';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Helpers:', () => {
    const mockAdapter = new axiosMockAdapter(axios);
    const FAKE_ENDPOINT = 'blah/'
    const url = `${FAKE_ENDPOINT}`
    const responseData = {
        id: 'fake transaction id'
    };
    const MOCK_ACTION = 'MOCK_ACTION';
    const mockAction = (responseData) => ({
        type: MOCK_ACTION,
        responseData
    });

    beforeAll(() => {
        mockAdapter.reset();
    });

    describe('fetchHelper', () => {
        it('should create MOCK_ACTION when fetching from url is done', async () => {
            expect.assertions(1);

            const expectedActions = [
                {
                    type: MOCK_ACTION,
                    responseData
                }
            ];

            const store = mockStore({ transaction: {} });

            mockAdapter.onGet(url).reply(200, responseData);

            await store.dispatch(fetchHelper(url, mockAction));

            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});
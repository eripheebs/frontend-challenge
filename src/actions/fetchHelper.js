import fetchData from '../api/fetchData';

const fetchHelper = (url, action) => async (dispatch) => {
    const response = await fetchData(url);
    const responseData = response ? response : {};

    return dispatch(action(responseData));
};

export default fetchHelper;
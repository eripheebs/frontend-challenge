import axios from 'axios';
import axiosMockAdapter from 'axios-mock-adapter';
import putData from './putData';

describe('API: put data', () => {
    const url = '/endpoint';
    const mockAdapter = new axiosMockAdapter(axios);
    const status = 'FAKE STATUS';
    const data = {
        status
    };

    beforeAll(() => {
        mockAdapter.reset();
    });

    test('should handle 200 responses with the correct response object', async () => {
        expect.assertions(1);

        const responseData = {
            data: 'Fake 200 data'
        };

        mockAdapter.onPut(url, data).reply(200, responseData);
        const response = await putData(url, status);
        expect(response).toEqual(responseData);
    });

    test('should return the correct error message for non-200 responses', async () => {
        expect.assertions(14);

        const errorCodes = [300, 301, 302, 304, 307, 400, 401, 403, 404, 410, 500, 501, 503, 550];
        const testErrorCode = async (code) => {
            mockAdapter.onPut(url, data).reply(code);

            try {
                await putData(url, status);
            } catch (error) {
                expect(error).toEqual(
                    new Error(`Request failed with status code ${code}`)
                );
            }
        };

        for (const code of errorCodes) {
            await testErrorCode(code);
        }
    });
});

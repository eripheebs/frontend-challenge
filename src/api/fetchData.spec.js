import axios from 'axios';
import axiosMockAdapter from 'axios-mock-adapter';
import fetchData from './fetchData';

describe('API: fetchData', () => {
    const url = '/endpoint';
    const mockAdapter = new axiosMockAdapter(axios);

    beforeAll(() => {
        mockAdapter.reset();
    });

    test('should handle 200 responses with the correct response object', async () => {
        expect.assertions(1);

        const responseData = {
            data: 'Fake data'
        };

        mockAdapter.onGet(url).reply(200, responseData);
        const response = await fetchData(url);
        expect(response).toEqual(responseData);
    });

    test('should handle 500 responses by returning undefined', async () => {
        expect.assertions(1);

        mockAdapter.onGet(url).reply(500);
        const response = await fetchData(url);
        expect(response).toEqual(undefined);
    });

    test('should return the correct error message for non-200 responses', async () => {
        expect.assertions(13);

        const errorCodes = [300, 301, 302, 304, 307, 400, 401, 403, 404, 410, 501, 503, 550];
        const testErrorCode = async (code) => {
            mockAdapter.onGet(url).reply(code);

            try {
                await fetchData(url);
            } catch (error) {
                expect(error).toEqual(
                    new Error(`Request failed with status code ${code}`)
                );
            }
        };

        for (const code of errorCodes) {
            await testErrorCode(code);
        }
    });
});

import axios from 'axios';

const handleError = (err) => {
    if (err.response.status === 500) {
        return undefined
    } else {
        throw new Error(err.message);
    }
}

const fetchData = async (url) => {
    try {
        const response = await axios.get(url);

        return response && response.data;
    } catch (err) {
        return handleError(err);
    }
}

export default fetchData;
import axios from 'axios';

const handleError = (err) => {
    throw new Error(err.message);
}

const putDataAxios = async (url, data, config) => {
    try {
        const response = await axios.put(url, data, config);

        return response;
    } catch (err) {
        handleError(err);
    }
}

const putData = async (url, status) => {
    const data =  {
        status: status
    }

    const config = {
        headers: {'Content-Type': 'application/json'}
    }

    const response = await putDataAxios(url, data, config);
    return response && response.data;
}

export default putData;
import transaction from './transaction';
import { GET_TRANSACTION } from '../constants/actionTypes';

describe('Reducer: transaction', () => {
    const initialState = {
        details: {}
    };

    it('should return the initial state', () => {
        expect(transaction(undefined, {})).toEqual(initialState);
    });

    it('should handle GET_TRANSACTION', () => {
        const details = 'Fake details';
        const reducerReturn = transaction(
            [],
            {
                type: GET_TRANSACTION,
                details
            }
        )
        const state = {details};
        expect(reducerReturn).toEqual(state);
    });
});
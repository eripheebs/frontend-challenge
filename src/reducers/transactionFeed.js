import { GET_TRANSACTIONS,
    ADD_TRANSACTIONS_TO_STATE,
    FILTER_TRANSACTONS
} from '../constants/actionTypes';
import statuses from '../constants/statuses';

const initialState = {
    transactions: [],
    page: 1,
    endOfScroll: false,
    filter: statuses.ALL
};

const actionHandlers = {
    [GET_TRANSACTIONS]: (state, action) => {
        const { transactions } = action;

        return {
            ...state,
            transactions
        };
    },
    [ADD_TRANSACTIONS_TO_STATE]: (state, action) => {
        const { transactions, page } = action;

        return {
            ...state,
            page: page,
            transactions: [...transactions, ...state.transactions],
            endOfScroll: !transactions.length
        };
    },
    [FILTER_TRANSACTONS]: (state, action) => {
        const { status } = action;

        return {
            ...state,
            filter: status,
            filteredTransactions: state.transactions.filter(
                transaction => transaction.status === status
            )
        }
    }
};

const transactionFeed = (state = initialState, action) => {
    const { type } = action;

    if (actionHandlers[type]) {
        return actionHandlers[type](state, action);
    }

    return state;
};

export default transactionFeed;

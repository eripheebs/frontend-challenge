import { combineReducers } from 'redux';
import transactionFeed from './transactionFeed';
import transaction from './transaction';
import user from './user';

const rootReducer = combineReducers({
    transaction,
    transactionFeed,
    user
});

export default rootReducer;
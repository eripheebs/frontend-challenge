import { GET_TRANSACTION } from '../constants/actionTypes';
import dateFormat from 'dateformat';

const initialState = {
    details: {}
};

const actionHandlers = {
    [GET_TRANSACTION]: (state, action) => {
        const { details } = action;

        return {
            ...state,
            details: formatDates(details)
        };
    }
};

const transaction = (state = initialState, action) => {
    const { type } = action;

    if (actionHandlers[type]) {
        return actionHandlers[type](state, action);
    }

    return state;
};

const formatDates = (details) => {
    if (details.fromDate) {
        details.fromDate = dateFormat(details.fromDate, 'dddd, mmmm dS, yyyy');
    }
    if (details.toDate) {
        details.toDate = dateFormat(details.toDate, 'dddd, mmmm dS, yyyy');
    }
    return details;
};

export default transaction;

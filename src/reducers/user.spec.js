import user from './user';
import { GET_USER } from '../constants/actionTypes';

describe('Reducer: transaction', () => {
    const initialState = {
        details: {}
    };

    it('should return the initial state', () => {
        expect(user(undefined, {})).toEqual(initialState);
    });

    it('should handle GET_USER', () => {
        const details = 'Fake details';
        const reducerReturn = user(
            [],
            {
                type: GET_USER,
                details
            }
        )
        const state = {details};
        expect(reducerReturn).toEqual(state);
    });
});
import transactionFeed from './transactionFeed';
import { GET_TRANSACTIONS,
    ADD_TRANSACTIONS_TO_STATE,
    FILTER_TRANSACTONS
} from "../constants/actionTypes";
import statuses from '../constants/statuses';

describe('Reducer: transactionFeed', () => {
    const page = 1;
    const endOfScroll = false;
    const initialState = {
        transactions: [],
        filter: statuses.ALL,
        endOfScroll,
        page
    };

    const status = 'Fake status';
    const transaction = {
        status
    };
    const transactions = [transaction];

    it('should return the initial state', () => {
        expect(transactionFeed(undefined, {})).toEqual(initialState);
    });

    it('should handle GET_TRANSACTIONS', () => {
        const reducerReturn = transactionFeed(
            [],
            {
                type: GET_TRANSACTIONS,
                transactions
            }
        )
        const state = {transactions};
        expect(reducerReturn).toEqual(state);
    });

    it('should handle ADD_TRANSACTIONS_TO_STATE', () => {
        const newTransactions = ['Fake transaction new'];
        const reducerReturn = transactionFeed(
            {
                transactions
            },
            {
                type: ADD_TRANSACTIONS_TO_STATE,
                transactions: newTransactions,
                endOfScroll,
                page
            }
        )
        const state = {
            transactions: [...newTransactions, ...transactions],
            endOfScroll,
            page
        };
        expect(reducerReturn).toEqual(state);
    });

    it('should handle FILTER_TRANSACTONS', () => {
        const filteredTransactions = [transaction];
        const reducerReturn = transactionFeed(
            {
                transactions
            },
            {
                type: FILTER_TRANSACTONS,
                status
            }
        )
        const state = {
            transactions,
            filter: status,
            filteredTransactions
        };
        expect(reducerReturn).toEqual(state);
    });
});
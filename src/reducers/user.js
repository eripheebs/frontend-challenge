import { GET_USER } from '../constants/actionTypes';

const initialState = {
    details: {}
};

const actionHandlers = {
    [GET_USER]: (state, action) => {
        const { details } = action;

        return {
            ...state,
            details
        };
    }
};

const user = (state = initialState, action) => {
    const { type } = action;

    if (actionHandlers[type]) {
        return actionHandlers[type](state, action);
    }

    return state;
};

export default user;

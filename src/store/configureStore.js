import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/rootReducer';

const configureStore = (initialState) => {
    const middleware = thunk;
    const reducer = rootReducer;

    return createStore(
        reducer,
        initialState,
        applyMiddleware(middleware)
    );
}

export default configureStore;

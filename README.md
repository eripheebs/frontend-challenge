# Frontend Challenge

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

### Set up
- Run the backend on localhost :8080

```bash
git clone https://eripheebs@bitbucket.org/eripheebs/frontend-challenge.git
cd frontend-challenge
npm install
npm run start-develop
```

### Tests
Test files are specified with `.spec.ts` extensions, and located next to the implementation they are testing.
```bash
npm run test
```

### File structure
```

public/
src/
|  +--actions/
|  +--api/
|  +--components/
    |  +--containers/
    |  +--presentation/
    |  +--Transactions/
    |  +--User/
|  +--constants/
|  +--img/
|  +--reducers/
|  +--scss/
|  +--store/
|  +--index.js
|  +--registerServiceWorker.js

```

### Brief explanation of app
- The landing page is the feed, where you can scroll down and see all the most recent transactions
- You can also filter the transactions by status
- You can view individual transactions by going to the endpoint /transaction/:id or clicking on it from the feed
- You can change the status of the transaction